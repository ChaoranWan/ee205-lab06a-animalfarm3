///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <random>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 3" << endl;
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for (int i = 0 ; i < 25 ; i++ ) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }
   cout << endl;
   cout << "Array of Animals:" << endl;
   switch (animalArray.empty()) {
      case 1: cout << "   Is it empty: true" << endl; break;
      case 0: cout << "   Is it empty: false" << endl; break;
   }
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;
/*   for( Animal* animal : animalArray ) {
      if (animalArray[i] == NULL) {
      }
      cout << animal->speak() << endl;
   }
*/
   for (int i = 0; i < 30; i++) {
      if (animalArray[i] == NULL) {
         break;
      }
      cout << animalArray[i]->speak() << endl;
   }

   //deconstructor
   for(Animal* animal : animalArray) {
      if(animal != NULL) {
         delete animal;
      }
   }

   list<Animal*> animalList;
      for (int i = 0 ; i < 25 ; i++ ) {
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   cout << endl << "List of Animals:" << endl;
      switch (animalList.empty()) {
      case 1: cout << "   Is it empty: true" << endl; break;
      case 0: cout << "   Is it empty: false" << endl; break;
   }
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   for( Animal* animal : animalList ) {
      cout << animal->speak() << endl;
   }

   //deconstructor
   for(Animal* animal : animalList) {
      if(animal != NULL) {
         delete animal;
      }
   }


//   cout << AnimalFactory::getRandomAnimal()->speak();
}
